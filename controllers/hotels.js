import fetch from 'node-fetch'
import url from 'url'
import { sort } from '../helpers/sort'
import { filter } from '../helpers/filter'

/**
 * 
 * @param {*} req express request obj
 * @param {*} res express response obj
 */
export const getHotels = (req, res) => {
  return fetch(`${process.env.DB_URL}/hotels`)
    .then(res => res.json())
    .then(data => {
      const queryParams = url.parse(req.url, true).query // Get query params from url
      const filterName = filter(data, 'name', queryParams.name) // Filter by name
      const filterStars = filter(filterName, 'stars', queryParams.stars) // Filter by stars
      const hotels = sort(filterStars, queryParams.sort) // Sort data
      return res.status(200).json(hotels) // Ok response with data
    })
    .catch(error => res.status(500).json(error)) // Error response
}

/**
 * 
 * @param {*} req express request obj
 * @param {*} res express request obj
 */
export const getHotel = (req, res) => {
  const id = req.params.hotelId
  return fetch(`${process.env.DB_URL}/hotels/${id}`)
    .then(res => res.json())
    .then(hotels => res.status(200).json(hotels))
    .catch(error => res.status(500).json(error))
}

export const addHotel = (req, res) => {
  return fetch(`${process.env.DB_URL}/hotels`, {
      method: 'POST',
      body: JSON.stringify(req.body),
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(res => res.json())
    .then(hotel => res.status(200).json(hotel))
    .catch(error => res.status(500).json(error))
}

export const deleteHotel = (req, res) => {
  return fetch(`${process.env.DB_URL}/hotels/${req.params.hotelId}`, {
      method: 'DELETE',
    }).then(res => res.json())
    .then(hotel => res.status(200).json({
      status: 'deleted'
    }))
    .catch(error => res.status(500).json(error))
}

export const updateHotel = (req, res) => {
  return fetch(`${process.env.DB_URL}/hotels/${req.params.hotelId}`, {
      method: 'PATCH',
      body: JSON.stringify(req.body),
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(res => res.json())
    .then(hotel => res.status(200).json(hotel))
    .catch(error => res.status(500).json(error))
}