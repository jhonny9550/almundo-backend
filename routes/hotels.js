import { Router } from 'express'
import * as hotelsCtrl from '../controllers/hotels'

const router = Router()

router.get('/', hotelsCtrl.getHotels)
router.get('/:hotelId', hotelsCtrl.getHotel)
router.post('/', hotelsCtrl.addHotel)
router.delete('/:hotelId', hotelsCtrl.deleteHotel)
router.patch('/:hotelId', hotelsCtrl.updateHotel)

export default router