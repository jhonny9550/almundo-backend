# API Rest NodeJS

Prueba realizada en NodeJS utilizando javascript ES6 para Almundo.com.

API con CRUD de hoteles y listado + filtro de hoteles.

Para verificar la funcionalidad del CRUD de hoteles, se pueden usar los siguientes endpoints:

    - GET: /v1/hotels
    - GET: /v1/hotels/<hotelId>
    - POST: /v1/hotels
    - PATCH: /v1/hotels/<hotelId>
    - DELETE: /v1/hotels/<hotelId>

Los datos quedarán guardados en el archivo *data.json* ubicado en la carpeta *./data*, se está usando json-server para emular el crud localmente, se recomienda tener los puertos 5000 y 3000 abiertos. La data original de la prueba está guardada en *./data/data_backup.json*

## Bajar el repositorio

Clonar el proyecto del repositorio de github usando el siguiente comando:

    $ git clone https://github.com/jhonny9550/almundo-backend.git

Ingresar a la carpeta del proyecto e instalar las dependencias

    $ cd almundo-backend
    $ npm install

## Emular

Para emular el proyecto se debe estar dentro de la carpeta del mismo y ejecutar:

    $ npm run serve

## Desplegar

Para desplegar el servicio se tiene el script de arranque _**npm start**_, por defecto se asigna la variable "*NODE_ENV*" con valor "*production*", pero se puede asignar desde la configuración del proveedor de infraestructura.

El script de arranque es:

    $ npm start

*Autor:* Jhonny Martínez

*Email:* jhonny9550@gmail.com

*Github:* github.com/jhonny9550  