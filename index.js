import express from 'express'
import bodyParser from 'body-parser'
import jsonServer from 'json-server'
import dotenv from 'dotenv'
import routes from './routes'

if (process.env.NODE_ENV !== 'production') {
  // Init Json Server
  dotenv.load()
  const server = jsonServer.create()
  const router = jsonServer.router('./data/data.json')
  const parser = jsonServer.bodyParser
  const middlewares = jsonServer.defaults()

  server.use(middlewares)
  server.use(router)
  server.use(parser)
  server.listen(3000, () => {
    console.log('JSON Server is running')
  })
}

const app = express()

// If production env, then allows only your apps domain, else allow any origin for code properly
if (process.env.NODE_ENV === 'production') {
  app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "myawesome.domain.com")
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
    next()
  })
} else {
  app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*")
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
    next()
  })
}

// Enable body parser to application-json | content-type
app.use(bodyParser.json())

// Use routes
app.use('/v1', routes)

// Testing endpoint
app.get('/', (req, res) => res.send('Hello world!'))

// Init server watch
app.listen(process.env.PORT, () => console.log('Server running on port 5000'))