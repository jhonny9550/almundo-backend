/**
 * 
 * @param {*} data object array data to filter
 * @param {*} field object prop name to be filter
 * @param {*} value value to filter
 */
export const filter = (data, field, value) => {
  if (!value) return data
  return data.filter(el => {
    if (field === 'name') {
      const regexp = new RegExp('^' + value, 'i')
      return regexp.test(el.name)
    }
    if (field === 'stars') {
      const stars = value.split(',').map(el => parseInt(el))
      return stars.indexOf(el.stars) !== -1
    }
  });
}
