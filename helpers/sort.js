/**
 * 
 * @param {*} data object array data to be sorted
 * @param {*} type custom sort type fomart. Example: name_asc | price_desc
 */
export const sort = (data, type) => {
  if (!type) return data
  const field = type.split('_')[0]
  const order = type.split('_')[1]
  if (order !== 'asc' && order !== 'desc') return data
  return data.sort((a, b) => {
    if (order === 'asc') {
      if (a[field] < b[field]) return -1
      if (a[field] > b[field]) return 1
    }
    if (order === 'desc') {
      if (a[field] > b[field]) return -1
      if (a[field] < b[field]) return 1
    }
    return 0
  })
}